import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/blocs/tema/tema_bloc.dart';
import 'package:weather_app/blocs/weather/weather_bloc.dart';
import 'package:weather_app/widget/gecisli_arkaplan_renk.dart';
import 'package:weather_app/widget/hava_durumu_resim.dart';
import 'package:weather_app/widget/location.dart';
import 'package:weather_app/widget/max_min_sicaklik.dart';
import 'package:weather_app/widget/sehir_sec.dart';
import 'package:weather_app/widget/son_guncelleme.dart';

class WeatherApp extends StatelessWidget {
  String secilenSehir = "Ankara";
  Completer<void> _refreshCompleter = Completer<void>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weather App"),
        actions: [
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () async {
                secilenSehir = await Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SehirSec()),
                );
                if (secilenSehir != null) {
                  context.bloc<WeatherBloc>().add(FetchWeatherEvent(sehirAdi: secilenSehir));
                }
              }),
        ],
      ),
      body: Center(
        child: BlocBuilder<WeatherBloc, WeatherState>(
          builder: (BuildContext context, state) {
            if (state is WeatherInitial) {
              return Center(
                child: Text("Şehir Seçiniz"),
              );
            } else if (state is WeatherLoadingState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (state is WeatherLoadedState) {
              final getirilenWeather = state.weather;
              secilenSehir = getirilenWeather.title;
              context.bloc<TemaBloc>().add(TemaDegistirEvent(havaDurumuKisaltmasi: getirilenWeather.consolidatedWeather[0].weatherStateAbbr));

              _refreshCompleter.complete();
              _refreshCompleter = Completer<void>();
              return BlocBuilder<TemaBloc, TemaState>(
                builder: (BuildContext context, state) {
                  return GecisliRenkContainer(
                    renk: (state as UygulamaTemasi).renk,
                    child: RefreshIndicator(
                      onRefresh: () {
                        context.bloc<WeatherBloc>().add(RefreshWeatherEvent(sehirAdi: secilenSehir));
                        return _refreshCompleter.future;
                      },
                      child: ListView(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: LocationWidget(
                                secilenSehir: getirilenWeather.title,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: SonGuncellemeWidget(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: HavaDurumuResimWidget(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: MaxMinSicaklik(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else if (state is WeatherErrorState) {
              return Center(
                child: Text("Hata OLuştu"),
              );
            }
          },
        ),
      ),
    );
  }
}
