part of 'tema_bloc.dart';

abstract class TemaState extends Equatable {
  const TemaState();
}

class UygulamaTemasi extends TemaState {
  final ThemeData tema;
  final MaterialColor renk;

  UygulamaTemasi({@required this.tema, @required this.renk});

  @override
  List<Object> get props => [this.tema, this.renk];
}
