part of 'weather_bloc.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent();
}

class FetchWeatherEvent extends WeatherEvent {
  final String sehirAdi;
  @override
  List<Object> get props => [this.sehirAdi];

  FetchWeatherEvent({@required this.sehirAdi});
}

class RefreshWeatherEvent extends WeatherEvent {
  final String sehirAdi;
  @override
  List<Object> get props => [this.sehirAdi];

  RefreshWeatherEvent({@required this.sehirAdi});
}
