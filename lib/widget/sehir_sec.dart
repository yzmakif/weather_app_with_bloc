import 'package:flutter/material.dart';

class SehirSec extends StatefulWidget {
  SehirSec({Key key}) : super(key: key);

  @override
  _SehirSecState createState() => _SehirSecState();
}

class _SehirSecState extends State<SehirSec> {
  final _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Şehir Seç"),
      ),
      body: Form(
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextFormField(
                  autofocus: true,
                  controller: _textController,
                  decoration: InputDecoration(
                    labelText: "Şehir",
                    hintText: "Şehir Seçin",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ),
            IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  Navigator.pop(context, _textController.text);
                })
          ],
        ),
      ),
    );
  }
}
