import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/blocs/weather/weather_bloc.dart';

class SonGuncellemeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (BuildContext context, state) {
        var yeniTarih = (state as WeatherLoadedState).weather.time.toLocal();
        return Text(
          "Son Guncelleme : ${TimeOfDay.fromDateTime(yeniTarih).format(context)}",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        );
      },
    );
  }
}
