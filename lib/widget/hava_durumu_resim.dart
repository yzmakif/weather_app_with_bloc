import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/blocs/weather/weather_bloc.dart';

class HavaDurumuResimWidget extends StatelessWidget {
  const HavaDurumuResimWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(builder: (BuildContext context, state) {
      var _weather = (state as WeatherLoadedState).weather;
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Text(
              _weather.consolidatedWeather[0].theTemp.floor().toString() + " °C",
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ),
          Image.network(
            "https://www.metaweather.com/static/img/weather/png/${_weather.consolidatedWeather[0].weatherStateAbbr}.png",
            width: 150,
            height: 150,
          ),
        ],
      );
    });
  }
}
