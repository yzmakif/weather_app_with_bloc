import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/data/weather_repository.dart';
import 'package:weather_app/locator.dart';
import 'package:weather_app/model/weather.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository weatherRepository = locator.get<WeatherRepository>();

  WeatherBloc() : super(WeatherInitial());

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    if (event is FetchWeatherEvent) {
      yield WeatherLoadingState();
      try {
        //hava Durumunu getir
        final Weather weather = await weatherRepository.getWeather(event.sehirAdi);
        yield WeatherLoadedState(weather: weather);
      } catch (e) {
        yield WeatherErrorState();
      }
    } else if (event is RefreshWeatherEvent) {
      //yield WeatherLoadingState();
      try {
        //hava Durumunu getir
        final Weather weather = await weatherRepository.getWeather(event.sehirAdi);
        yield WeatherLoadedState(weather: weather);
      } catch (e) {
        yield state;
      }
    }
  }
}
