import 'package:weather_app/data/weather_api_client.dart';
import 'package:weather_app/locator.dart';
import 'package:weather_app/model/weather.dart';

class WeatherRepository {
  WeatherApiClient weatherApiClient = locator.get<WeatherApiClient>();
  Future<Weather> getWeather(String sehir) async {
    final int sehirId = await weatherApiClient.getLocationId(sehir);
    return await weatherApiClient.getWeather(sehirId);
  }
}
