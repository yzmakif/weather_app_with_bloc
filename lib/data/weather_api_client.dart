import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:weather_app/model/weather.dart';

class WeatherApiClient {
  static const baseUrl = "https://www.metaweather.com";
  final http.Client httpClient = http.Client();

  Future<int> getLocationId(String sehirAdi) async {
    final sehirUrl = "$baseUrl/api/location/search/?query=$sehirAdi";
    final gelenCevap = await httpClient.get(sehirUrl);

    if (gelenCevap.statusCode != 200) {
      throw Exception("Veri Getirilemedi");
    }

    final gelenCevapJson = (jsonDecode(gelenCevap.body)) as List;
    return gelenCevapJson[0]["woeid"];
  }

  Future<Weather> getWeather(int sehirId) async {
    final havaDurumuUrl = "$baseUrl/api/location/$sehirId";
    final havaDurumuGelenCevap = await httpClient.get(havaDurumuUrl);

    if (havaDurumuGelenCevap.statusCode != 200) {
      throw Exception("Hava Durumu Getirilemedi");
    }

    final gelenCevapJson = jsonDecode(havaDurumuGelenCevap.body);
    return Weather.fromJson(gelenCevapJson);
  }
}
