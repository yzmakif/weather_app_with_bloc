import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/blocs/weather/weather_bloc.dart';

class MaxMinSicaklik extends StatelessWidget {
  const MaxMinSicaklik({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (BuildContext context, state) {
        var _weather = (state as WeatherLoadedState).weather;
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              "Max : ${_weather.consolidatedWeather[0].maxTemp.floor()} °C",
              style: TextStyle(fontSize: 20),
            ),
            Text(
              "Min : ${_weather.consolidatedWeather[0].minTemp.floor()} °C",
              style: TextStyle(fontSize: 20),
            ),
          ],
        );
      },
    );
  }
}
