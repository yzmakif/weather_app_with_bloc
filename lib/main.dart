import 'package:flutter/material.dart';
import 'package:weather_app/blocs/tema/tema_bloc.dart';
import 'package:weather_app/blocs/weather/weather_bloc.dart';
import 'package:weather_app/locator.dart';
import 'package:weather_app/widget/weather_app.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  setupLocator();
  runApp(BlocProvider<TemaBloc>(create: (BuildContext context) => TemaBloc(), child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TemaBloc, TemaState>(
      builder: (BuildContext context, state) {
        return MaterialApp(
          builder: (context, child) => MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child),
          title: 'Weather App',
          debugShowCheckedModeBanner: false,
          theme: (state as UygulamaTemasi).tema,
          home: BlocProvider<WeatherBloc>(
            create: (BuildContext context) => WeatherBloc(),
            child: WeatherApp(),
          ),
        );
      },
    );
  }
}
